package INF102.lab5.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        if (!graph.hasNode(u) || !graph.hasNode(v)) {
        return false;
    }

    // Create a queue to store the nodes to explore.
    Queue<V> queue = new LinkedList<>();
    // Create a set to keep track of visited nodes.
    Set<V> visited = new HashSet<>();

    // Add the starting node to the queue and mark it as visited.
    queue.add(u);
    visited.add(u);

    // While the queue is not empty, explore the next node in the queue.
    while (!queue.isEmpty()) {
        V currentNode = queue.poll();

        // If the current node is the destination node, return true.
        if (currentNode.equals(v)) {
            return true;
        }

        // Add all the unvisited neighbors of the current node to the queue.
        for (V neighbor : graph.getNeighbourhood(currentNode)) {
            if (!visited.contains(neighbor)) {
                queue.add(neighbor);
                visited.add(neighbor);
            }
        }
    }

    // If the queue is empty, the destination node was not found, so return false.
    return false;
    }
}
